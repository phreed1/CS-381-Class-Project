# s/simple-wireless-tdma/serial/g
# s/tdma-central-mac/serial-central/g
# s/tdma-controller-helper/serial-controller-helper/g
# s/tdma-controller/serial-controller/g
# s/tdma-helper/serial-helper/g
# s/tdma-mac-low/serial-low/g
# s/tdma-mac-net-device/serial-net-device/g
# s/tdma-mac-queue/serial-queue/g
# s/tdma-mac/serial/g
# s/tdma-example/serial-example/g
# s/Tdma/Serial/g
# s/tdma/serial/g
# s/TDMA/SERIAL/g
# s/CENTRALIZED/SENTRALIZED/g
# s/Hemanth Narra <hemanthnarra222@gmail.com>/Fred Eisele <phreed@gmail.com>/g
# find . \( -name '*.cc' -or -name '*.h' \) -exec sed -f ./update.sed -i {} \;   
# s/SimpleWirelessChannel/SerialChannel/g
# s/Copyright (c) 2010 Vanderbilt/Copyright (c) 2011 Vanderbilt University/g
# s/Copyright (c) 2011 Hemanth Narra/Copyright (c) 2011 Vanderbilt University/g

s/Hemanth Narra <hemanth@ittc.ku.com>/Fred Eisele <phreed@gmail.com>/g
s/Copyright (c) 2010 Hemanth Narra/Copyright (c) 2011 Vanderbilt University/g

